let vm = new Vue({
    el: '#app',
    data: {
        histories: []
    },
    mounted: function (){
        this.$http.get('/api/history').then(response => {
            this.histories = response.body.data;
        });
    },
    computed: {
        classObject () {
            return this.histories.map( (b) => {
                if(b.status=='sukses')
                    return 'badge badge-success'
                if(b.status=='gagal')
                    return 'badge badge-danger'
                else
                    return 'badge badge-secondary'
            })
        }
    },
    methods: {
        transaksi: function() {
            let tujuan = $('#handphone').val();
            let nominal = $('#nominal').val();
            this.$http.post('/api/transaksi', {nominal: nominal, tujuan: tujuan}).then(response => {
                this.histories.unshift(response.body.data);
                $('#exampleModal').modal('hide')
            });
        },
        updateStatus: function(index, history, newstatus){
            // console.log(history);
            this.$http.patch('/api/update/'+history.id, {status: newstatus}).then(response => {
                
                if(newstatus=='batal')
                    this.histories.splice(index, 1)
                else
                    if(response.body.status=='suskes')
                        this.histories[index].status = newstatus
                    else
                        alert('transaksi tidak dapat dirubah');
            });
        }
    }
})