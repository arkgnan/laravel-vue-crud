<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Histories;
use App\Http\Resources\HistoryResource;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gethistory = Histories::orderBy('created_at', 'DESC')->get();

        return HistoryResource::collection($gethistory);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nominal' => ['required'],
            'tujuan' => ['required', 'min:8']
        ]);

        $history = Histories::create([
            'nominal'=>request('nominal'),
            'tujuan'=>request('tujuan'),
            'status'=>'diproses',
            'sn' => ''
        ]);
        return new HistoryResource($history);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $history = Histories::find($id);
        if(request('status')=='batal')
        {
            $this->destroy($id);
            return ['status'=>'sukses'];
        } else {
            if($history->status=='diproses')
            {
                $sn = uniqid();
                $history->update([
                    'status' => request('status'),
                    'sn' => $sn
                ]);
                return new HistoryResource($history);
            } else {
                return ['status'=>'gagal'];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Histories::find($id)->delete();
        return ['status'=>'success'];
    }
}
