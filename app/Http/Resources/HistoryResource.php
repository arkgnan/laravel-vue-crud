<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class HistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "nominal"=>$this->nominal,
            "tujuan"=>$this->tujuan,
            "sn"=>$this->sn,
            "status"=>$this->status,
            "tanggal"=>Carbon::parse($this->created_at)->format('d M Y H:i'),
        ];
    }

    public function with($request)
    {
        return ['status'=>'success'];
    }
}
