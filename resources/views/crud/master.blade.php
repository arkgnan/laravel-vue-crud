<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRUD</title>
    <link rel="stylesheet" href="{{ asset('library/bootstrap-4.5.2-dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('library/font-awesome-4.7.0/css/font-awesome.min.css') }}">
</head>
<body>
    <div id="app">
        @yield('content')
    </div>    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('library/jquery-3.5.1.slim/jquery-3.5.1.slim.min.js') }}"></script>
    <script src="{{ asset('library/popper.js-1.6/popper.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-4.5.2-dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('library/vue-2.6.11/vue.js') }}"></script>
    <script src="{{ asset('library/vue-resource-1.5.1/vue-resource.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
</body>
</html>