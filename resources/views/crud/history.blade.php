@extends('crud/master')
@section('content')
<div class="container-fluid">
    <div class="row p-3">
        <div class="col-8">
            <h3>History Transaksi Pulsa</h3>
        </div>
        <div class="col-4 text-right">
            <button type="button" class="btn btn-primary btn-sm " data-toggle="modal" data-target="#exampleModal">
                <i class="fa fa-plus"></i> Isi pulsa
            </button>
        </div>
        <div class="col-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Nominal</th>
                    <th scope="col">Tujuan</th>
                    <th scope="col">SN</th>
                    <th scope="col">Status</th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(history, index) in histories" :key="history.id">
                        <th scope="row">@{{ history.tanggal }}</th>
                        <td>@{{ history.nominal }}</td>
                        <td>@{{ history.tujuan }}</td>
                        <td>@{{ history.sn }}</td>
                        <td><span :class="classObject">@{{ history.status }}</span></td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#" @click="updateStatus(index, history,'sukses')">Sukseskan</a>
                                    <a class="dropdown-item" href="#" @click="updateStatus(index, history,'gagal')">Gagalkan</a>
                                    <a class="dropdown-item" href="#" @click="updateStatus(index, history,'batal')">Batalkan</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Transaksi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="handphone">No handphone</label>
                    <input type="text" class="form-control" id="handphone" placeholder="0858123456">
                </div>
                <div class="form-group">
                    <label for="nominal">Nominal</label>
                    <select class="form-control" id="nominal">
                        <option value="" selected>-- pilih nominal--</option>
                        <option value="5000">5.000</option>
                        <option value="10000">10.000</option>
                        <option value="25000">25.000</option>
                        <option value="50000">50.000</option>
                        <option value="100000">100.000</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" @click="transaksi">Process</button>
        </div>
        </div>
    </div>
</div>
@endsection