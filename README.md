Tugas CRUD laravel dengan vue  
simulasi laporan transaksi pulsa yang biasanya ada di halaman webreport untuk agen distribusi pulsa all operator 

- saat pertama kali input statusnya diproses  
- update status menjadi sukses atau gagal hanya jika statusnya masih diproses, case seperti ini sering terjadi saat ada gangguan dari sisi supplier sehingga status transaksi menggantung, makanya perlu disukseskan atau digagalkan manual.  
- aksi batalkan akan menghapus data yang sudah diinput
